import java.io.*;
import java.util.Scanner;

//Global Variabel
class Data
{
	static int input;
	static int factorial1;
	static int factorial2;
	
	public Data()
	{
		this.input = input;
		this.factorial1 = factorial1;
		this.factorial2 = factorial2;
	}
}

// Thread Class
class thread implements Runnable
{
	Data data = new Data();
	private Thread t;
	private String threadName;
	private int no;
	
	public int factorial = 1;
	
	thread(String name, int n)
	{
		no = n;
		threadName = name;
		System.out.println("Creating " + threadName);
	}
	
	// Run function
	public void run() 
	{
		System.out.println("Running " + threadName);
		
		int start = 0;
		int finish = 0;
	
		if(no == 1) 
		{ 
			start = data.input;
			finish = data.factorial2;
		}
		else if (no == 2) 
		{
			start = data.factorial2;
			finish = 0;
		}
		
		for (int i = start; i > finish; i--)
		{
			factorial *= i;
		}
		System.out.println("Factorial from thread " + no + " : "+ factorial);
	}
	
	// Start function
	public void start()
	{
		System.out.println("Starting " + threadName);
		if (t == null)
		{
			t = new Thread (this, threadName);
			t.start();
		}
	}
	
	// Join function
	public void join() 
	{
		try 
		{
			t.join();
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
}

public class Main
{
	public static void main(String[] args)
	{
		Data data = new Data();
		
		// Scanner input data
		System.out.println("Enter factorial : ");
		Scanner scan = new Scanner(System.in);
		data.input = scan.nextInt();
		
		// Process each thread
		data.factorial1 = data.input / 2;
		data.factorial2 = data.input - data.factorial1;
		
		thread T1 = new thread("Thread - 1", 1);
		thread T2 = new thread("Thread - 2", 2);
		
		// Call Thread to run process
		T1.start();
		T2.start();
		
		//Next Thread wait until the first thread finish
		T1.join();
		T2.join();
		
		//Display result
		int result = T1.factorial * T2.factorial;
		System.out.println("Result: " + result);
		
	}
}